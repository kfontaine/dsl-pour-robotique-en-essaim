Require Import Pactole.Setting Pactole.Util.FSets.FSetInterface.
Require Import Pactole.Observations.LimitedSetObservation.
Require Import Pactole.Models.Rigid Pactole.Models.Similarity.
Require Import Pactole.Spaces.Ring Reals Pactole.Spaces.Isomorphism.
Section test.
  Variable n: nat.
  Instance MyRobots: Names := Robots (7 * n) n.
   
  Instance MyRing: RingSpec :=
    {| ring_size := 5;
      ring_size_spec:= ltac:(auto) |}.
  Notation ring_node := (finite_node ring_size).
  Instance Loc: Location := make_Location ring_node.
   
  Inductive light:= Toto | Rouge.
  Definition info := (location * light)%type.
  Instance St : State info := AddInfo light (OnlyLocation (fun f => sigT (fun sim : similarity location => Bijection.section sim == f))).
  Variable r: R.
  Instance Obs: Observation := limited_set_observation VS r.
   
  Instance RC: robot_choice location :=
    { robot_choice_Setoid := location_Setoid}.
  Instance UC: update_choice unit := NoChoice.
  Instance FC: frame_choice (Z * bool) :=
     {frame_choice_bijection :=
        fun nb => if snd nb then Ring.sym (fst nb) else Ring.trans (fst nb);
      frame_choice_Setoid := eq_setoid _ }.
  Instance UpdFun: update_function direction (Z * bool) unit :=
    {
      update := fun config g _ dir _ => move_along (config (Good g)) dir;
      update_compat := ltac:(repeat intro; subst; now apply move_along_compat) }.
  Instance IC : inactive_choice unit := NoChoiceIna.
  Instance InaFun : inactive_function unit := NoChoiceInaFun.
   
  Definition robogram_pgm (s : observation): location :=
    let c := (elements s)
    in match c with
       | nil => (0,0)
       | cons pt nil => pt
       | cons _ (cons _ _) => isobarycenter c
       end.
  Instance robogram_pgm_compat : Proper (equiv ==> equiv) robogram_pgm.
  (* Proof *)
  Admitted.
  Definition robogram: robogram :=
    {| pgm := robogram_pgm; pgm_compat := robogram_pgm_compat |}.
   
  (* Definition of phase *)
  Definition F (config : configuration):= (* To_Complete *).
  Definition C (config : configuration):= (* To_Complete *).
  Definition E (config : configuration):= (* To_Complete *).
  Definition D (config : configuration):= (* To_Complete *).
  Definition B (config : configuration):= (* To_Complete *).
  Definition A (config : configuration):= (* To_Complete *).
  (* Definition of measure per phase *)
  Definition F_meas (config : configuration): nat := (* To_Complete *).
  Definition C_meas (config : configuration): nat := (* To_Complete *).
  Definition E_meas (config : configuration): nat := (* To_Complete *).
  Definition D_meas (config : configuration): nat := (* To_Complete *).
  Definition B_meas (config : configuration): nat := (* To_Complete *).
  Definition A_meas (config : configuration): nat := (* To_Complete *).
  Lemma InGraph: forall config,
                   (C config) \/
                     (E config) \/ (D config) \/ (B config) \/ (A config).
  Proof.
    (* TODO *)
  Qed.
  Lemma A_next_B da: forall config,
                       A config = true -> B (round robogram da config) = true.
  Proof.
    (* TODO *)
  Qed.
  Lemma B_next_F_or_D_or_C da: forall config,
                                 B config = true ->
                                   (F (round robogram da config) = true \/
                                      D (round robogram da config) = true \/
                                        C (round robogram da config) = true).
  Proof.
    (* TODO *)
  Qed.
  Lemma D_next_E da: forall config,
                       D config = true -> E (round robogram da config) = true.
  Proof.
    (* TODO *)
  Qed.
  Lemma E_next_F_or_C da: forall config,
                            E config = true ->
                              (F (round robogram da config) = true \/
                                 C (round robogram da config) = true).
  Proof.
    (* TODO *)
  Qed.
  Lemma C_next_F_or_E da: forall config,
                            C config = true ->
                              (F (round robogram da config) = true \/
                                 E (round robogram da config) = true).
  Proof.
    (* TODO *)
  Qed.
  Definition measure (config : configuration): nat*nat :=
    if A config then (5, A_meas config)
      else if B config then (4, B_meas config)
             else if D config then (3, D_meas config)
                    else if E config then (2, E_meas config)
                           else if C config then (1, C_meas config)
                                  else (0, F_meas config).
  Instance measure_compat : Proper (equiv ==> Logic.eq) measure.
  (* Proof *)
  Admitted.
   
  Definition lt_config x y := Lexprod.lexprod lt lt (measure (!! x)) (measure (!! y)).
  Lemma wf_lt_config: well_founded lt_config.
  Proof.
    (* TODO *)
  Qed.
  Instance lt_config_compat : Proper (equiv ==> equiv ==> iff) lt_config.
  (* Proof *)
  Admitted.
  Theorem round_lt_config : forall config,
    moving robogram da config <> nil ->
    lt_config (round robogram da config) config.
End test.
