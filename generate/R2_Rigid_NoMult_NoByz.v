Require Import Pactole.Setting Pactole.Util.FSets.FSetInterface.
Require Import Pactole.Models.NoByzantine.
Require Import Pactole.Observations.SetObservation.
Require Import Pactole.Models.Rigid.
Require Import Pactole.Spaces.R2 Rbase.
Section R2_Rigid_NoMult_NoByz.
  Variable n: nat.
  Instance MyRobots: Names := Robots n 0.
  Instance NoByz : NoByzantine.
  Proof using . now split. Qed.
   
  Instance Loc: Location := make_Location R2.
  Instance VS: RealVectorSpace location := R2_VS.
  Instance ES: EuclideanSpace location := R2_ES.
   
  Instance St: State location := OnlyLocation (fun _ => True).
  Instance Obs: Observation := set_observation.
   
  Instance RobotChoice: robot_choice location :=
    { robot_choice_Setoid := location_Setoid}.
  Instance ActiveChoice: update_choice unit := NoChoice.
  Instance UpdateFunc:
    update_function (location) (Similarity.similarity location) unit :=
    {update := fun _ _ _ target _ => target;update_compat := ltac:(now repeat intro) }.
  Instance Rigid : RigidSetting.
  Proof using . split. reflexivity. Qed.
  Instance InactiveChoice : inactive_choice unit := NoChoiceIna.
  Instance InactiveFunc : inactive_function unit := NoChoiceInaFun.
   
  Definition Robogram_pgm (s : observation): location :=
    let c := elements s
    in match c with
       | nil => (0,0)
       | cons pt nil => pt
       | cons _ (cons _ _) => isobarycenter c
       end.
  Instance Robogram_pgm_compat : Proper (equiv ==> equiv) Robogram_pgm.
  (* Preuve *)
  Definition Robogram: robogram :=
    {| pgm := Robogram_pgm; pgm_compat := Robogram_pgm_compat |}.
End R2_Rigid_NoMult_NoByz.
