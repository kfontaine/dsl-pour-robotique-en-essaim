Require Import Pactole.Setting Pactole.Util.FSets.FSetInterface.
Require Import Pactole.Models.NoByzantine.
Require Import Pactole.Observations.SetObservation.
Require Import Pactole.Models.Flexible.
Require Import Pactole.Spaces.R2 Rbase.
Section R2_Flexible.
  Variable n: nat.
  Instance MyRobots: Names := Robots n 0.
  Instance NoByz : NoByzantine.
  Proof using . now split. Qed.
   
  Instance Loc: Location := make_Location R2.
  Instance VS: RealVectorSpace location := R2_VS.
  Instance ES: EuclideanSpace location := R2_ES.
  Definition path_R2:= path location.
  Definition paths_in_R2: location -> path_R2 := local_straight_path.
  Coercion paths_in_R2 :location >-> path_R2.
   
  Instance St: State location := OnlyLocation (fun _ => True).
  Instance Obs: Observation := set_observation.
   
  Instance RobotChoice: robot_choice (path location) :=
    { robot_choice_Setoid := path_Setoid location}.
  Instance ActiveChoice: update_choice ratio := Flexible.OnlyFlexible.
  Variable delta: R.
  Instance UpdateFunc:
    update_function (path location) (Similarity.similarity location) ratio :=
      FlexibleUpdate delta.
  Instance InactiveChoice : inactive_choice unit := NoChoiceIna.
  Instance InactiveFunc : inactive_function unit := NoChoiceInaFun.
   
  Definition Robogram_pgm (s : observation): (path location) :=
    let aux s := let c := elements s
                 in match c with
                    | nil => (0,0)
                    | cons pt nil => pt
                    | cons _ (cons _ _) => isobarycenter c
                    end
    in paths_in_R2 (aux s).
  Instance Robogram_pgm_compat : Proper (equiv ==> equiv) Robogram_pgm.
  (* Preuve *)
  Definition Robogram: robogram :=
    {| pgm := Robogram_pgm; pgm_compat := Robogram_pgm_compat |}.
End R2_Flexible.
