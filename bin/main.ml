open Dsl


let usage = "Usage: dsl filename_dsl [-proof filename_proof] [-type filename_type] [-r repository_out] [-n] [-g] [-h] \n -n : don't overwrite type file \n -g : generate png from graph"

(** [parse_args argrs proof_name type_name owerwrite repo_gen] Parses the command line arguments and returns a tuple containing the parsed values or default value.
    @param args string list: The list of command line arguments.
    @param proof_name string: The name of the proof file.
    @param type_name string: The name of the type file.
    @param overwrite : A boolean indicating whether to overwrite the type file.
    @param dir_gen string: The name of the output repository.
    @return A tuple containing the parsed values or default value passed as an argument in thr first function's call: (proof_name, type_name, overwrite, repo_gen).
*)
let rec parse_args args (proof_name : string) (type_name : string) (overwrite: bool) (dir_gen : string) (graph_gen:bool):(string * string * bool * string *bool)= 
    match args with
      | "-proof" :: proof :: tl -> parse_args tl proof type_name overwrite dir_gen graph_gen
      | "-type" :: type_ :: tl -> parse_args tl proof_name type_ overwrite dir_gen graph_gen
      | "-n" :: tl -> parse_args tl proof_name type_name (not overwrite) dir_gen graph_gen
      | "-r" :: dir :: tl -> parse_args tl proof_name type_name overwrite dir graph_gen
      | "-g" :: tl -> parse_args tl proof_name type_name overwrite dir_gen (not graph_gen)
      | "-h" :: _ -> print_endline usage; exit 0
      | _ :: tl ->parse_args tl proof_name type_name overwrite dir_gen graph_gen
      | [] -> (proof_name, type_name, overwrite,dir_gen,graph_gen) 




(** [gen args] function that takes a string array [args] as input and generates Coq files based on the provided arguments.
    @param args: string array, array of command line arguments
    *)
let gen (args : string array) = 
  
  if Array.length args < 2 then (print_endline usage; exit 0)
  else 
    let list_args =   Array.to_list args in
    let name = args.(1) in
    let (proof_name, type_name, overwrite,repo_gen, graph_gen) = parse_args list_args ((Filename.basename name)^"_proof") ((Filename.basename name)^"_world_type") true "." false in 
    Gencoq.generate_coq_2files (Interface.parse_description name) (Filename.basename name) proof_name type_name overwrite repo_gen graph_gen
    ;;

gen Sys.argv;;
