(** This defines an AST for coq statements (coq expressions being formulas
   (Formula.t)). The main goal of this library is to serve as a target type for
   programs generating coq files, with pretty printing facilities.  *)


type stmt =
  | Var of string * Formula.t
  | VarImplicit of string * Formula.t
  | VarMaxImplicit of string * Formula.t
  | Hyp of string * Formula.t
  | Lemma of string * Formula.t * stmt list (* tactics *)
  | Parameter of string * Formula.t *Formula.t (* Module *)
  | Parameterlem of string * Formula.t * stmt list (* Module *)
  | InstanceLemma of string * Formula.t * stmt list (* tactics *)
  | Def of string * Formula.t * Formula.t
  | Notation of string * Formula.t
  | Function of string * Formula.t (* typ *) * Formula.t(* struct *) * Formula.t
  | Fixpoint of string * Formula.t (* type *) * Formula.t(* struct *) * Formula.t
  | Instance of string * Formula.t * Formula.t
  | ExistingInstance of Formula.t
  | Section of string * stmt list
  | Module of string * stmt list
  | ModuleType of string * stmt list
  | ModuleImpType  of string * string * stmt list
  | Proof of stmt list
  | Require of bool * string list (* bool = Import *)
  | Import of string list
  | RawCoq of string list
  | Local of stmt
  | Global of stmt
  | Comment of string (* comment delimiters + line wrapping *)
  | Formul of Formula.t (* a simple formula, like a rawcoq but with structure *)
(* On enlève ça pour ne pas dépendre de trop de librairies. *)
(* [@@deriving show,ord] (*,yojson { strict = true }*) *)

(* let newline2 fmt () = Format.fprintf fmt "@\n@\n" *)

let rec pretty fmt st =
  let p = Format.fprintf in
  let prtyf = Formula.pretty in
  match st with
  | Var(s,f) -> p fmt "@[<hov 2>Variable %s:@ %a.@]" s prtyf f
  | VarImplicit(s,f) -> p fmt "@[<hov 2>Context {%s:@ %a }. @]" s prtyf f
  | VarMaxImplicit(s,f) -> p fmt "@[<hov 2>Context `{%s:@ %a }.@]" s prtyf f
  | Hyp(s,f) -> p fmt "@[<hov 2>Hypothesis %s:@ %a.@]" s prtyf f
  | Lemma(s,f,lst) ->
     p fmt "@[<v 0>@[<hov 2>Lemma %s: %a.@]@,@[<v 2>Proof.@,%a@]@,Qed.@]"
       s prtyf f (Pp.print_list Pp.brk pretty) lst
  | Parameter(s,v,r) -> p fmt "@[<hov 2>@[<hov 2>Parameter %s:@ %a@] ->@ %a.@]"
    s prtyf v prtyf r 
  | Parameterlem(s,f,_) -> p fmt "@[<v 0>@[<hov 2>Parameter %s: %a.@]"
      s prtyf f 
  | InstanceLemma(s,f,lst) ->
     p fmt "@[<v 0>@[<hov 2>Instance %s: %a.@]@,@[<v 2>Proof.@,%a@]@,Defined.@]"
       s prtyf f (Pp.print_list Pp.brk pretty) lst
  | Def(s,ftyp,f) -> p fmt "@[<hov 2>@[<hov 2>Definition %s:@ %a@] :=@ %a.@]"
                       s prtyf ftyp prtyf f
  | Notation(s,f) -> p fmt "@[<hov 2>Notation %s :=@ (%a).@]" s prtyf f
  | Function(s,ftyp,struc,f) -> p fmt "@[<hov 2>@[<hov 2>Function %s:@ %a@ { %a }@] :=@ %a.@]"
                                  s prtyf ftyp prtyf struc prtyf f
  | Fixpoint(s,ftyp,struc,f) -> p fmt "@[<hov 2>@[<hov 2>Fixpoint %s:@ %a@ { %a }@] :=@ %a.@]"
                                  s prtyf ftyp prtyf struc prtyf f
  | Instance(s,ftyp,f) -> p fmt "@[<hov 2>@[<hov 2>#[export] Instance %s:@ %a@] :=@ %a.@]"
                            s prtyf ftyp prtyf f
  | ExistingInstance(f) -> p fmt "@[<hov 2>@[<hov 2>Existing Instance %a.@]@]" prtyf f
  | Section (s,lst) -> p fmt "@[<v 0>@[<v 2>Section %s.@,%a@]@\nEnd %s.@]"
                         s (Pp.print_list Pp.cut pretty) lst s
  | Module (s,lst) ->  p fmt "@[<v 0>@[<v 2>Module %s.@,%a@]@\nEnd %s.@]"
  s (Pp.print_list Pp.cut pretty) lst s
  | ModuleType (s,lst) ->  p fmt "@[<v 0>@[<v 2>Module Type %s_type.@,%a@]@\nEnd %s_type.@]"
  s (Pp.print_list Pp.cut pretty) lst s
  | ModuleImpType (s,st,lst) ->  p fmt "@[<v 0>@[<v 2>Module %s : %s.@,%a@]@\nEnd %s.@]"
  s st (Pp.print_list Pp.cut pretty) lst s
  | Proof (lst) -> p fmt "@[<v 0>@[<v 2>Proof.@,%a@]@\nEnd.@]"
                     (Pp.print_list Pp.cut pretty) lst
  | Require (_,[]) -> ()
  | Require (b,ls) -> p fmt "@[<2>Require %a.@]" (Pp.print_list Pp.space Pp.print_string)
                        (if b then "Import"::ls else ls)
  | Import [] ->  ()
  | Import ls ->  p fmt "Import %a." (Pp.print_list Pp.space Pp.print_string) ls
  | RawCoq [] -> ()
  | RawCoq ls -> p fmt "%a" (Pp.print_list Pp.cut Pp.print_string) ls
  | Local s -> p fmt "@[<hov 2>Local@ %a@]" pretty s
  | Global s -> p fmt "@[<hov 2>Global@ %a@]" pretty s
  | Formul(f) -> p fmt "@[<hov 2>%a.@]" prtyf f
  | Comment s ->
     let reg = Str.regexp_string in
     p fmt "@[(* @[<v>%a@]@] *)" (Pp.print_string_lines (reg "\n") (reg " ")) s

let pretty_l fmt lstmt =
  Format.fprintf fmt "@[<v>%a@]" (Pp.print_list Pp.brk pretty) lstmt

