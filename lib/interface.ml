(** [parse f filename] is a function which takes a function [f] and a character string [filename] as parameters. 
    It uses a lexical analyzer to convert the character string into a stream of tokens,
    then use the [f] function to parse the tokens and return the result. 
    If a parsing error occurs, an exception of type [Failure] is thrown with the message "Parse error". 
    @param [f] token analysis function
    @param [filename] string corresponding to the path of the file to be parsed.
    @return The syntactic tree resulting from the parsing.*)

let parse_file f filename =
  let in_channel = open_in filename in 
  let lexbuf = Lexing.from_channel in_channel in
  try
    let result = f Lexer.token lexbuf in
    Lexing.set_filename lexbuf filename;
    close_in in_channel;
    result
  with Parser.Error ->
    close_in in_channel;
    close_in in_channel;
    let pos = Lexing.lexeme_start_p lexbuf in
    Printf.eprintf "Error at line %d, column %d: Problem close to %s \n"
      pos.Lexing.pos_lnum
      (pos.Lexing.pos_cnum - pos.Lexing.pos_bol)
      (Lexing.lexeme lexbuf);
    exit 1

let parse_description filename =
  parse_file Parser.description filename
