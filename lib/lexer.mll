{
    open Parser
    let numero_ligne = ref 1 (*Permet la mutation, numero_ligne permet juste remonter la ligne qui pose une erreur*)
    let numero_carac = ref 1
}

let digit = ['0' - '9']
let integer = digit+
let float = integer '.' integer
let alpha = ['a'-'z' 'A'-'Z']
let space = [' ' '\t']+
let variable = (alpha | integer | '_')+
let keyword = ['A'-'Z']alpha*
rule token = parse 
    | "World"           {WORLD}
    | "Robogram"        {ROBOGRAM}
    | "Measure"         {MEASURE}
    | "Sync"            {SYNC}
    | "FSYNC"           {FSYNC}
    | "SSYNC"           {SSYNC}
    | "ASYNC"           {ASYNC}
    | "Space"           {SPACE}
    | "R"               {R}
    | "mR"              {MR}
    | "R2"              {R2}
    | "mR2"             {MR2}
    | "Ring"            {RING}
    | "Byzantine"       {BYZANTINE}
    | "Sensors"         {SENSORS}
    | "Robot"           {ROBOT}
    | "Roles"           {ROLES}
    | "ActivelyUpdate"  {ACTIVELYUP}
    | "Private"         {PRIVATE}
    | "Public"          {PUBLIC}
    | "Range"           {RANGE}
    | "Full"            {FULL}
    | "Limited"         {LIMITED}
    | "K_rand"          {K_RAND}
    | "K_enemy"         {K_ENEMY}
    | "Multiplicity"    {MULTI}
    | "No"              {NO_MEM}
    | "Weak Local"      {WEAKL}
    | "Strong Local"    {STRONGL}
    | "Weak Global"     {WEAKG}
    | "Strong Global"   {STRONGG}
    | "Light"           {LIGHT}
    | "Internal"        {INTERNE}
    | "External"        {EXTERNE}
    (*| "Red"             {RED}
    | "Blue"            {BLUE}
    | "Yellow"          {YELLOW}
    | "Green"           {GREEN}
    | "Orange"          {ORANGE}
    | "Purple"          {PURPLE}*)
    | "Opacity"         {OPACITY}
    | "Opaque"          {OPAQUE}
    | "Transparent"     {TRANSPARENT}
    | "Rigidity"        {RIGIDITY}
    | "Rigid"          {RIGIDE}
    | "Flexible"        {FLEXIBLE}
    | "Location"        {LOCATION}
    | "Direction"       {DIRECTION}
    | "Share"           {SHARE}
    | "Fullcompass"     {FULLCOMPASS}
    | "Chirality"       {CHIRALITY}
    | "1-Axe Orientation" {ONEAXEORIEN}
    | "["               { BRACKETOPEN}
    | "]"               { BRACKETCLOSE}
    | "."               { POINT}
    | ":"               { COLON}
    | ";"               { SEMICOLON}
    | "("               { PARENOPEN}
    | ")"               { PARENCLOSE}
    | "if"              { IF}
    | "then"            { THEN}
    | "else"            { ELSE}
    | "true" | "false"  { BOOLEAN (bool_of_string (Lexing.lexeme lexbuf))}
    | "match"           { MATCH}
    | "with"            { WITH}
    | "|"               { PIPE}
    | "end"             { END}
    | "->"              { ARROW}
    | "let"             { LET}
    | "="               { AFFECT}
    | "in"              { IN}
    | ":="              { IS}
    | '+'               { PLUS}
    | '-'               { MINUS}
    | '*'               { MULT}
    | '/'               { DIV}
    | "&&"              { AND}
    | "||"              { OR}
    | '>'               { SUP}
    | '<'               { INF}
    | "=="              { EQUAL}
    | "!="              { DIFF}
    | "!"               { NOT}
    | "fun"             { FUN}
    | "/*"              { comment lexbuf}
    | integer           { INT (int_of_string (Lexing.lexeme lexbuf))}
    | float             { FLOAT (float_of_string(Lexing.lexeme lexbuf) )}
    | ","               { COMMA}
    | keyword           { KEYWORD (Lexing.lexeme lexbuf)}
    | variable          { VARIABLE (Lexing.lexeme lexbuf)}
    | space             {   let pos = lexbuf.Lexing.lex_curr_p in
                            lexbuf.Lexing.lex_curr_p <- { pos with Lexing.pos_cnum = pos.Lexing.pos_cnum + 1; };
                            token lexbuf}
    | "\n"              { let pos = lexbuf.Lexing.lex_curr_p in
                          lexbuf.Lexing.lex_curr_p <- { pos with Lexing.pos_lnum = pos.Lexing.pos_lnum + 1; Lexing.pos_bol = pos.Lexing.pos_cnum; };
                          token lexbuf
                        }
    | eof {EOF}


and comment = parse 
    | "*/" {token lexbuf}
    | "\n" {let pos = lexbuf.Lexing.lex_curr_p in
                          lexbuf.Lexing.lex_curr_p <- { pos with Lexing.pos_lnum = pos.Lexing.pos_lnum + 1; Lexing.pos_bol = pos.Lexing.pos_cnum; };
                           comment lexbuf}
    | _  {comment lexbuf}
