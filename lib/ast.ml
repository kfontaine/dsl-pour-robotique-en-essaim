
(*ROBOGRAM*)

type binop = 
  | Plus
  | Minus
  | Mult
  | Div
  | And
  | Or
  | Sup 
  | Inf 
  | Equal
  | Diff

(*tail::head au lieu de head::tail pour éviter les empilements inutiles
   (((vide,x),y),z)  
   pour le set x,y,z*)
type 'a set = 
  | Vide
  | Some of ('a set) * 'a 


type expr =
  | None 
  | Bool of bool 
  | Cst of int 
  | Cstf of float
  | BinOp of expr * binop * expr
  | Not of expr
	| Cond of expr * expr  *expr
  | Var of string 
  | Let of string * expr * expr
  | Fun of string * expr
  | App of expr * expr
  | Affectation of string * expr  
  | Set of expr set 
  | Point of string * string *int (* int : line to raise error*)
  | PatternMatching of expr * (expr * expr)list
  | Cons of expr *expr 
  



(*DESCRIPTION DE L'ENVIRONNEMENT*)

(** type synchro: 
    Fsync -> Full synchronized
    Ssync -> Semi synchronized
    Async -> Asynchronized*)
type synchro = 
  | Fsync
  | Ssync
  | Async

(** type space: 
    R -> one line
    Rm -> a line millefeuille (convoy)
    R2 -> a plan
    R2m -> plan millefeuille (convoy)
    ZnZ -> ring taking n -> congruent positions
    *)
type space =
  | R
  | MR of int option
  | R2 
  | MR2 of int option
  | ZnZ of int option

(* type range:
   Full -> Full range
   Limited -> range limited to a delta
   K_rand -> range limited to a delta + capable of seeing k robots beyond delta chosen random
   K_enemy -> range limited to a delta + capable of seeing k robots beyond delta chosen by an enemy*)
type range =
  | Full
  | Limited of expr
  | K_rand of expr * expr 
  | K_enemy of expr * expr

(* type multiplicity:
   No -> No multiplicity
   Weak -> Low multiplicity: knows if 1 or more robots but not how many robots
   Strong -> Strong multiplicity: knows the number of robots 
   Local -> robot only knows its position
   Global -> robot knows for all observed positions
   *)
type multiplicite =
  | No 
  | Weak_Local
  | Strong_Local
  | Weak_Global
  | Strong_Global


type color =
  | Red
  | Blue
  | Yellow
  | Green
  | Purple
  | Orange


(* type light: List of colors * line to raise error (int)
    Intern -> Light seen by the robot itself
    Extern -> Light seen by robots other than himself
    All -> Light seen by all robots including themselves *)
type light =
  | Intern of string list * int
  | Extern of string list *int
  | All of string list *int


(* type opacity:
    Opaque -> Robots are opaque 
    Transparent -> Robots are transparent *)
type opacity = 
	| Opaque
	| Transparent

(* type rigidity:
    Rigid -> the robots reach their final destination, they are not interrupted during their movement
    Flexible of float -> Robots can be interrupted during their movement, they do not necessarily reach their 
                        final destinations however it travels at least a delta distance *)
type rigidity =
  | Rigide 
  | Flexible of expr

(* type representing degradation functions *)  
type degrfunc =
  | Str of string
  | Fun of expr

(* type sensor: robot's sensors *)
type sensor = 
  | Range of range
  | Opacity of opacity
  | Multi of multiplicite
  | Custom of string * degrfunc

(* value type: value that can be used in field fields*)
type value = 
  | Empty  
  | Int of int 
  | Float of float
  | String of string
  | Liste of value list
  | Light of light

(* type field: type corresponding to the field that the user will fill in for the robot *)
type field = 
  | Private of (string * value)
  | Public of (string * value)

type byzantine = 
  | Number of int
  | Id of int list

type valretR =
  | Location
  | Direction
  | Str of string

type share =
  | Fullcompass
  | Chirality
  | DirectionShare
  | OneAxisOrientation

type information =
  | Sync of synchro
  | Rigidity of rigidity
  | Space of space
  | Byzantines of byzantine
  | Sensors of sensor list
  | Robot of field list
  | ActivelyUpdate of valretR list
  | Roles of (int * string) list
  | Share of share


(*STRING CONVERSION*)
let string_of_valretr (v : valretR) :string =
  match v with
  |Location -> "Location"
  |Direction -> "Direction"
  |Str s -> s
let rec string_of_set (s: 'a set) (f : 'a -> string) : string =
  match s with 
  | Vide -> "()"
  | Some (s',e) -> "("^ string_of_set s' f ^"," ^ f e ^")"

(** [string_of_expression e] Allows you to obtain a string corresponding to the expression [e]
    @param [e] expression whose string we want
    @return string corresponding to the expression [e]
    *)
let rec string_of_expression (e : expr) : string =
  match e with
  | None -> "None"
  | Bool b -> string_of_bool b
  | Cst i -> string_of_int i
  | Cstf f -> string_of_float f
  | BinOp(x,op ,y) -> (match op with
                      | Plus  -> "("^string_of_expression(x)^ " + " ^ string_of_expression(y)^")" 
                      | Minus -> "("^string_of_expression(x)^ " - " ^string_of_expression(y)^")" 
                      | Mult  -> "("^string_of_expression(x)^" * " ^string_of_expression(y)^")" 
                      | Div   -> "("^string_of_expression(x)^ " / " ^string_of_expression(y)^")" 
                      | And   -> string_of_expression(x) ^ " && " ^string_of_expression(y) 
                      | Or    -> string_of_expression(x) ^ " || " ^string_of_expression(y)
                      | Sup   -> "("^string_of_expression(x) ^ " > "^string_of_expression(y)^")" 
                      | Inf   -> "("^string_of_expression(x) ^ " < " ^string_of_expression(y)^")" 
                      | Equal -> string_of_expression(x) ^ " == " ^string_of_expression(y)
                      | Diff  -> string_of_expression(x) ^ " != " ^string_of_expression(y)
                      )
  | Not b -> "!("^string_of_expression b^")" 
  | Var v ->  v
  | Cond(c,e1,e2) -> "if (" ^string_of_expression c ^ ") then \n {" ^ string_of_expression e1 ^ "} \n else {" ^ string_of_expression e2 ^ "}"
  | Affectation(v,e) ->  v ^ " = " ^ string_of_expression e
  | Set s -> string_of_set s string_of_expression 
  | Let (v,e1,e2) -> "Let  " ^ v ^ " = " ^ string_of_expression e1 ^ " in \n(" ^string_of_expression e2 ^")"
  | Fun (s,e) -> "Function  " ^ s ^ " = (" ^ string_of_expression e ^")"
  | App (f,arg) -> "("^string_of_expression f ^" "^ string_of_expression arg^")" 
  | Point (k,v,_) -> "value  " ^ v^ " of  "^ k ^""
  | PatternMatching (m,l) -> "match "^string_of_expression m^" with \n   | "^ String.concat "\n   | "(List.map (fun e -> match e with |
                                                                            e1,e2 -> string_of_expression e1 ^" => "^string_of_expression e2 ) 
                                                                                                       l)^ "\nend"
  | Cons (head,tail) -> string_of_expression head ^ " :: " ^ string_of_expression tail

let string_of_espace (e:space) : string =
match e with  
  | R -> "R"
  | MR _ -> "mR"
  | R2 -> "R2"
  | MR2 _  ->"mR2"
  | ZnZ _ -> "Ring"
  
let string_of_color (c : color) : string =
  match c with
  | Red     -> "Red"
  | Blue    -> "Blue"
  | Yellow  -> "Yellow"
  | Green   -> "Green"
  | Purple  -> "Purple"
  | Orange  -> "Orange"



let string_of_rigidity (r: rigidity) : string =
  match r with
  | Rigide -> "Rigide"
  | Flexible _ -> "Flexible"



(** [info_sync i] verifies that an information is information on synchronization
    @param i information
    @return bool *)
let info_sync (i : information):bool = 
  match i with
  | Sync _ -> true
  | _      -> false
    
(** [info_espace i] verifies that information is information about space
    @param i information
    @return bool *)
let info_space (i : information):bool = 
  match i with
  | Space _ -> true
  | _        -> false

let info_robot (i : information):bool = 
  match i with
  | Robot _ -> true
  | _        -> false

let compar_expr (e1 : expr) (e2: expr) :int =
  match e1,e2 with 
  | Var x1,Var x2 -> String.compare x1 x2
  | _ -> -1

(*GRAPH*)
module Node = struct                                                                
  type t = expr                                                                     
  let compare = compar_expr                                                 
  let hash = Hashtbl.hash                                                          
  let equal = (=)                                                                  
end                                                                                 

module Edge = struct                                                                
  type t = string                                                                  
  let compare = compare                                                                   
  let default = ""                                                                 
end


module G =  Graph.Imperative.Digraph.ConcreteLabeled(Node)(Edge)
module Topological = Graph.Topological.Make(G)
module D = Graph.Traverse.Dfs(G)
module T = Graph.Components.Make(G)

type edge =
| Nolab of (expr * expr)


(**[graph_from_edges el] creates a graph from a list of edges [el] 
    @param edge [list]
    @return [G.t]*)
let graph_from_edges (el : edge list)=

  let g = G.create () in
  let nolab = fun (x, y) -> 
    let vx = G.V.create x in 
    let vy = G.V.create y in
    G.add_edge g vx vy
  in 
  let rec add_edge (el : edge list) = 
    match el with
    | [] -> ()
    | (Nolab h)::t -> nolab h ; add_edge t
  in add_edge el;
  g 

(**[png_from_graph g name] generates a png file of the graph [g]
    @param g [G.t]
    @param name [string] png file name 
    @return[unit]*)
let png_from_graph g name dir =
  let graph = G.fold_vertex (fun v acc ->
    let succs = List.map string_of_expression (G.succ g v)in
    let v' = string_of_expression v in
      let edges = List.fold_left (fun acc s ->
          acc ^ "\"" ^ v' ^ "\" -> \"" ^ s ^ "\";\n") "" succs in
      acc ^ "\"" ^ v' ^ "\";\n" ^ edges
    ) g "" in
  let dot = "digraph G {\nrankdir=LR;\nranksep=1;\n" ^ graph ^"\nsplines=true;\noverlap=false; "^"}" in
  let oc = open_out (dir^"/"^name^"_measure.dot") in
  Printf.fprintf oc "%s\n" dot;
  close_out oc;
  ignore (Sys.command ("dot -Tpng "^dir^"/"^name^"_measure.dot -o "^dir^"/"^ name^"_measure.png"))

(**[assign_heigth g] returns the phase, phase weight association list. The phase weight is deduced from the order
                     topology of the phase
    @param G.t the graph in which we must assign the weights to the vertices
    @return [(expr * int) list]*)
let assign_height (g : G.t) : (expr * int) list =
  (*let total_nodes = G.nb_vertex g in*)
  let rec assign_order nodes counter order =
    match nodes with
    | [] -> order
    | h :: tl -> assign_order tl (counter +1) ((h, counter) :: order)
  in
  let nodes = Topological.fold (fun node acc  -> node :: acc) g [] in
  assign_order nodes 0 []


    

module NodeMap = Map.Make(Node)

(** [update_lowlink v w lowlink] updates the lowlink value for node [v] based on node [w].*)
let update_lowlink v w lowlink =
  let v_lowlink = NodeMap.find v lowlink in
  let w_lowlink = NodeMap.find w lowlink in
  NodeMap.add v (min v_lowlink w_lowlink) lowlink

(** [update_on_stack w on_stack] remove node [w] from list [on_stack]. *)
  let update_on_stack w on_stack =
    List.filter (fun v -> v <>  w) on_stack

  
(** [pop_stack v stack on_stack scc] removes elements from the [stack] until reaching node [v]. 
    This updates [on_stack] by removing nodes removed from the [stack], and [scc] by adding these nodes to the new strongly connected component.*)   
     let rec pop_stack v stack on_stack scc =
      match stack with
      | w :: rest -> if w <> v then pop_stack v rest (update_on_stack w on_stack) (w :: scc)
                     else rest, (update_on_stack w on_stack), (w :: scc)
      | [] -> raise(Failure "Error: stack empty")
    

(** [strongconnect v index stack indices lowlink on_stack sccs g] is the function that performs the dfs to find strongly connected components. 
    For each successor [w] of [v] in graph [g], if [w] is not yet visited, it is recursively visited.
    After visiting all successors, if [v] is a root of SCC, all nodes in the stack up to [v] are added to a new SCC.. 
    *)
let rec strongconnect (g : G.t) (v:expr) (index:int) stack indices lowlink on_stack sccs =
  let indices = NodeMap.add v index indices in
  let lowlink = NodeMap.add v index lowlink in
  let stack = v :: stack in
  let on_stack = v :: on_stack in
  let index = index + 1 in

  let index, stack, indices, lowlink, on_stack, sccs =
    G.fold_succ ( fun w (index, stack, indices, lowlink, on_stack, sccs) ->
      let v_lowlink = NodeMap.find v lowlink in
      if not (NodeMap.mem w indices) then (
        let index, stack, indices, lowlink, on_stack, sccs = strongconnect g w index stack indices lowlink on_stack sccs in
        let w_lowlink = NodeMap.find w lowlink in
        let lowlink = NodeMap.add v (min v_lowlink w_lowlink) lowlink in
        (index, stack, indices, lowlink, on_stack, sccs)
      ) else if List.mem w on_stack then (
        let w_index = NodeMap.find w indices in
        let lowlink = NodeMap.add v (min v_lowlink w_index) lowlink in
        index, stack, indices, lowlink, on_stack, sccs
      ) else (
        index, stack, indices, lowlink, on_stack, sccs
      )
    ) g v (index,stack,indices,lowlink,on_stack,sccs) in

    if NodeMap.find v lowlink = NodeMap.find v indices then
      let stack, on_stack, scc = pop_stack v stack on_stack [] in
      let sccs = scc :: sccs in
      index, stack, indices, lowlink, on_stack, sccs
    else
      index, stack, indices, lowlink, on_stack, sccs
  
(** [tarjan g]  is the main function that initializes the data structures and initiates dfs . 
                and returns the list of strongly related components *)
let tarjan g =
     (*Tarjan : *)
      let _, _, _, _, _, sccs =
        G.fold_vertex (fun v (index,stack,indices,lowlink,on_stack,sccs) ->
          if not (NodeMap.mem v indices) then
            strongconnect g v index stack indices lowlink on_stack sccs
    else
            (index, stack, indices, lowlink, on_stack, sccs)
        ) g (0,[],NodeMap.empty,NodeMap.empty,[],[])
      in
      sccs 


   module SCC = struct
    type t = expr list
    let compare = Stdlib.compare
    let hash = Hashtbl.hash
    let equal = (=)
  end 

  module G2 = Graph.Imperative.Digraph.ConcreteLabeled(SCC)(Edge) 

  (**[string_of_expr_list expr_list] returns a character string from a list of expressions such as [[X;Y]] corresponding to: string_of_expression X, string_of_expression Y, .... *)
let string_of_expr_list expr_list =
  let string_list = List.map string_of_expression expr_list in
  String.concat ", " string_list    

(**[png_from_graph2 g ] renvoie l'image au format png du graphe [g] 
    @param g de type [G2.t] graphe de liste de liste d'expression, composants fortement connexe *)  
let png_from_graph2 (g:G2.t) (name : string) (dir : string) :unit =
  let graph = G2.fold_vertex (fun v acc ->
    let succs = List.map string_of_expr_list (G2.succ g v)in
    let v' = string_of_expr_list v in
      let edges = List.fold_left (fun acc s ->
          acc ^ "\"" ^ v' ^ "\" -> \"" ^ s ^ "\";\n") "" succs in
      acc ^ "\"" ^ v' ^ "\";\n" ^ edges
    ) g "" in
  let dot = "digraph G {\nrankdir=LR;\nranksep=1;\n" ^ graph ^"\nsplines=true;\noverlap=false; "^"}" in
  let oc = open_out (dir^"/"^name^"_measure.dot") in
  Printf.fprintf oc "%s\n" dot;
  close_out oc;
  ignore (Sys.command ("dot -Tpng "^dir^"/"^name^"_measure.dot -o "^dir^"/"^ name^"_measure.png"))
  

(** [scc_graph g] returns the graph G2.t corresponding to the strongly connected component graph of graph [g]
                  by retrieving the list of strongly connected components and adding them to the created G2.t graph[scc_graph].
                  Then the function iterates over each SCC and adds edges between the SCCs based on the original graph [g]. 
                  For each vertex in an SCC, it iterates over its successors and finds the SCC to which each successor belongs. 
                  If the vertex SCC and the successor SCC are different, an edge is added between the corresponding SCCs in [scc_graph].

    @param g [G.t]
    @return [scc_graph] of type [G2.t], the strongly connected component graph of [g] *)
let scc_graph g =
  let scc_list = tarjan g in
  let scc_graph = G2.create () in
  List.iter (fun scc -> G2.add_vertex scc_graph scc) scc_list;
  let add_edges scc =
    List.iter (fun v ->
      G.iter_succ (fun succ ->
        let succ_scc = List.find (List.exists (fun v' -> v' = succ)) scc_list in
        if scc <> succ_scc then
          G2.add_edge scc_graph scc succ_scc;
      ) g v;
    ) scc ;
  in
  List.iter add_edges scc_list;
  scc_graph;;




type measure = Measure of G.t
(* A description is 1: description of the environment and the robot's capabilities
                        2: robogram which is the distributed algorithm
                        3: a graph representing the measure*)
type description = Description of (information list) * (expr list) * measure

  

(**[get_sensors d] Returns the list of sensors contained in description [d]
    @param description 
    @return [sensor list]
    *)
let get_sensors (d : description) : sensor list =
  match d with 
  | Description(d', _,_) -> (match List.find_opt (fun x -> match x with Sensors _ -> true | _-> false) d' with 
                          Some (Sensors s) -> s 
                          | _ -> raise(Failure("Error: Not find Sensors")))

(**[get_robot d] Returns the list of robot fields contained in the description [d]
    @param description
    @return [field list]*)
let get_robot (Description(d', _,_) : description) : field list =
  match List.find_opt (fun x -> match x with Robot _ -> true | _-> false) d' with 
                          Some (Robot r) -> r 
                          | _ -> raise(Failure("Error: Not find Robot"))

(**[get_activelyup d] Returns the list of strings corresponding to the fields updated by the robogram
    @param description
    @return [string list] *)
let get_activelyup (d : description) : string list = 
  match d with 
  | Description(d', _,_) -> (match List.find_opt (fun x -> match x with ActivelyUpdate _ -> true | _-> false) d' with 
                          Some (ActivelyUpdate a) -> List.map string_of_valretr a
                          | _ -> raise(Failure("Error: Not find ActivelyUpdate")))

(** [check_sensor s r] verifies that a sensor [s] is indeed declared in the robot [r], or that it is not a custom sensor 
    @param [s] sensor to be checked
    @param [r] field list corresponding to the robot in which we check [s]
    @return true if [s] is not custom or it is custom and contained in [r]
*)
let check_sensor (s: sensor) (r:field list):bool =
    let only_public flist = List.filter (fun x -> match x with Public _ -> true| _-> false) flist in
    let field_to_pair f = List.map(fun x -> match x with Public x -> x | Private x -> x) f in
    match s with 
    | Custom (s',_)-> List.exists (fun (x,_) -> x = s') (field_to_pair (only_public r))
    | _ -> true

(** [check_sensors_desc d] checks that the list of sensors in the description matches robot sensors
    @param d description in which we check its list of sensors with its robot declaration (field list)
    @return [true] if the sensors are custom and are in robot or if they are not custom*)
let check_sensors_desc (d:description) :bool =
   let res= List.find_opt (fun x -> not (check_sensor x (get_robot d))) (get_sensors d) in
    match res with 
    | Some x -> raise(Failure("Error : " ^(match x with |Custom (x',_)-> x' | _->"")^" is not declared public in robot"))
    | _-> true

(**[check_activelyup d] verifies that the fields entered in activelyupdate are declared in robot
    @param d [description]
    @return [bool]*)
let check_activelyup (d:description) :bool =
    let field_to_pair f = List.map(fun x -> match x with Public x -> x | Private x -> x) f in
    let aux s l = List.exists (fun (x,_) -> x = s) (field_to_pair l) in
    let fields_to_check f = List.filter (fun x -> (x <> "Location") && (x <> "Direction")) f ;in
    let res = List.find_opt (fun x -> not (aux x (get_robot d))) (fields_to_check(get_activelyup d)) in
    match res with 
    | Some x -> raise(Failure("Error : " ^x^" is not declared in robot"))
    | _-> true
  

(**[check_light d] if there is internal light we check that it is not indicated in public
                   and if extern not in private
    @param description [d] description of the world
    @return bool true if no internal light in public *)
let check_light d : bool = 
  let rec check flist = (
    fun x -> match x with 
    | [] -> true
    | Public (_,Light(Intern (_ ,line)))::_ | Private (_, Light(Extern (_ ,line)))::_ -> raise(Failure("Error line "^string_of_int line^" light in wrong field"))
    | _::t-> check t
   ) flist 
  in
  check (get_robot d)



(**[check_description d] verifies that the description [d] has the minimum information
                          expected for a description and for each type of information to be unique
                          (two definitions of space which one to choose?...)
    @param [d] description to check
    @return true if the description meets the criteria false otherwise
    *)
    let check_description (d : description) :bool =
			let unique f d = List.length (List.filter f d) = 1
			in 
      let aux d =
			match d with 
			| Description (d',_,_) -> if not(unique info_sync d')
                                  then raise(Failure "Error: Problem of defining sync")
													      else (
                                  if not(unique info_space d') 
                                    then raise(Failure "Error: Problem of defining space")
                                else (
                                  if not(unique info_robot d') 
                                    then raise(Failure "Error: Problem of defining sensors")
													        else true)
														  ) 
      in if  check_light d && check_activelyup d && check_sensors_desc d && aux d then true else false                   
;;
                              
                                