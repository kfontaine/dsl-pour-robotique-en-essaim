module StringMap = Map.Make(String)

let geometry = StringMap.empty
  |> StringMap.add "isobarycenter" (Formula.Raw "isobarycenter")
  |> StringMap.add "barycenter" (Formula.Raw "barycenter")
  |> StringMap.add "on_SEC" (Formula.Raw "on_SEC")

let observation = StringMap.empty
  |> StringMap.add "elements" (Formula.Raw "(elements s)")
  |> StringMap.add "support" (Formula.Raw "support s")

let direction= StringMap.empty
  |> StringMap.add "forward" (Formula.Raw "Forward")
  |> StringMap.add "backward" (Formula.Raw "Backward")
  |> StringMap.add "selfloop" (Formula.Raw "Selfloop")

let correspondance = StringMap.empty
  |> StringMap.add "Geometry" geometry
  |> StringMap.add "Obs" observation
  |> StringMap.add "Directions" direction