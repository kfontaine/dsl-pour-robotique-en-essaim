%{
    open Ast
%}

%token EOF
%token <int> INT
%token <float> FLOAT
%token IS
%token SYNC FSYNC SSYNC ASYNC
%token SPACE R MR R2 MR2 RING 
%token BYZANTINE
%token SENSORS
%token RANGE FULL LIMITED K_RAND K_ENEMY
%token MULTI NO_MEM WEAKL STRONGL WEAKG STRONGG
%token LIGHT INTERNE EXTERNE 
%token OPACITY OPAQUE TRANSPARENT
%token SHARE FULLCOMPASS CHIRALITY ONEAXEORIEN 
%token RIGIDITY RIGIDE FLEXIBLE 
%token COLON BRACKETOPEN BRACKETCLOSE SEMICOLON PARENOPEN PARENCLOSE COMMA
%token POINT
%token ROBOT PUBLIC PRIVATE ACTIVELYUP
%token ROLES
%token WORLD ROBOGRAM MEASURE


%token LOCATION DIRECTION

%token <bool> BOOLEAN 
%token <string> VARIABLE 
%token <string> KEYWORD
%token IF THEN ELSE MATCH WITH PIPE ARROW END
%token LET AFFECT IN FUN
%token PLUS MINUS MULT DIV AND OR NOT
%token SUP INF EQUAL DIFF

%nonassoc AFFECT 
%nonassoc SUP INF EQUAL DIFF 
%left PLUS MINUS
%left MULT DIV
%left OR
%left AND
%nonassoc NOT
%right VARIABLE KEYWORD PARENOPEN INT FLOAT BOOLEAN 

%type <Ast.description> description

%start description

%%

description : 
    | WORLD COLON information* 
      ROBOGRAM COLON robogram
      MEASURE COLON measure
      EOF {Description (List.flatten $3,$6,$9)}


/* DESCRIPTION DE L'ENVIRONNEMENT*/
information :
    /* Sync := s   */
    | SYNC IS s = sync {[Sync s]}
    /*  Rigidity := r */
    | RIGIDITY IS r = rigidite {[Rigidity r]}
    /* Space := e  */
    | SPACE IS e = espace {[Space e]}
    /* Byzantine := integer*/
    | BYZANTINE IS i = INT {[Byzantines (Number i)]}
    /* Byzantine := [1;2;8;9] list of ids corresponding to Byzantines*/
    | BYZANTINE IS id = list_int {[Byzantines (Id id)]}
    /* Roles := 0 : Compagnon; 5 */
    | ROLES IS r = separated_list(SEMICOLON,elements_list_role) {[Roles r]}/*Vraiment utile? avec robot pas sur..*/
    | robot sensors activelyupdate {[$1;$2;$3]}
    | robot activelyupdate sensors  {[$1;$2;$3]}
    | SHARE IS s = share_item {[Share s]}

robot : 
    /* Robot := Private ....
                Public .... */
    | ROBOT IS r = list_field {Robot r}
sensors:
    /* Sensors := ..;.. */
    | SENSORS IS l = separated_list(SEMICOLON,sensor) {Sensors l}
activelyupdate : 
    | ACTIVELYUP IS l = separated_list(SEMICOLON,valret)  {ActivelyUpdate l}

sensorname:
    | VARIABLE {$1}
    | KEYWORD {$1}
    | LIGHT {"Light"}
valret : 
    | sensorname {Str $1}
    | LOCATION {Location}
    | DIRECTION {Direction}

/*list of robot fields*/
list_field :
    | PRIVATE COLON l1 = separated_list(SEMICOLON,elements_private) PUBLIC COLON l2 = separated_list(SEMICOLON,elements_public) {l1@l2}
elements_private :
    | field {Private $1}
elements_public :
    | field {Public $1}

elements_list_role:
    |  id = INT COLON r = VARIABLE  {id,r}
    |  id = INT COLON r = KEYWORD  {id,r}

list_int : 
    | BRACKETOPEN ints BRACKETCLOSE {$2} 
ints : 
    | {[]}
    | INT {$1::[]}
    | INT SEMICOLON ints   {$1::$3}
%inline sync :
    /*Fully synchro*/
    | FSYNC {Fsync}
    /*Semi Synchro*/
    | SSYNC {Ssync}
    /*Assynchro*/
    | ASYNC {Async}

%inline espace :
    /* Space R */
    | R {R}
    /* Space R millefeuille*/
    | MR n = INT {MR(Some n)}
    | MR {MR(None)}
    /* Space RxR */
    | R2 {R2}
    /* Space RxR  millefeuille*/
    | MR2 n = INT {MR2(Some n)}
    | MR2 {MR2(None)}
    /*Ring Z/nZ */
    | RING n = INT {ZnZ(Some n)}
    | RING {ZnZ(None)}

%inline sensor : 
    /*Multi : m*/
    |  MULTI COLON m = multi {Multi m}
    /*Range : r */
    |  RANGE COLON r = range {Range r}
    /*Opacite : o*/
    |  OPACITY COLON o = opacite  {Opacity o}
    |  s=sensorname COLON f = VARIABLE {Custom (s,Fun (Var f))}
    |  s=sensorname COLON PARENOPEN f = deffun PARENCLOSE {Custom (s,Fun f)}
   


%inline field :
    | s = KEYWORD v = value {(s,v)}
    | s = VARIABLE v = value {(s,v)}
    | l = lights {("Light",l)} 


value_list :  
    | value {$1::[]}
    | value SEMICOLON value_list   {$1::$3}
value :
    | {Empty}
    | i = INT {Int i}
    | f = FLOAT {Float f}
    | s = VARIABLE {String s}
    | BRACKETOPEN value_list BRACKETCLOSE {Liste $2}



lights:
    /*Internal light -> visible only to the robot*/
    | INTERNE LIGHT BRACKETOPEN i = separated_list(SEMICOLON,KEYWORD) BRACKETCLOSE {Light(Intern (i,$startpos.Lexing.pos_lnum))}
    /*External light -> visible only to other robots*/
    | EXTERNE LIGHT BRACKETOPEN i = separated_list(SEMICOLON,KEYWORD) BRACKETCLOSE {Light(Extern (i,$startpos.Lexing.pos_lnum))}
    /*Light visible to all*/
    | FULL LIGHT BRACKETOPEN i = separated_list(SEMICOLON,KEYWORD) BRACKETCLOSE {Light(All (i,$startpos.Lexing.pos_lnum))}
 

%inline range :
    /* Full Range  */
    | FULL {Full}
    /* Limited Range to r */
    | LIMITED r = VARIABLE {Limited (Var r)}
    /* Limit to a distance r and k, a random number, of robot beyond r */ 
    | K_RAND r = VARIABLE k = VARIABLE {K_rand(Var r,Var k)} 
    /* Limit to a distance r and k, a number chosen by the enemy, from robot beyond r */ 
    | K_ENEMY r = VARIABLE k = VARIABLE {K_enemy(Var r,Var k)} 

%inline multi :
    /* No multiplicity */
    | NO_MEM {No}
    /* Weak local multiplicity -> 1 robot or robots where I am */
    | WEAKL {Weak_Local}
    /* String local multiplicity -> knows the number of robots where I am */
    | STRONGL {Strong_Local}
    /* Weak global multiplicity -> 1 robot or robots everywhere */
    | WEAKG {Weak_Global}
    /*Strong local multiplicity -> knows the number of robots everywhere*/
    | STRONGG {Strong_Global}


%inline opacite:
    /* Other robots can't see behind him */
    | OPAQUE {Opaque}
    /* Other robots can see behind him */
    | TRANSPARENT {Transparent}

%inline rigidite:
    | RIGIDE {Rigide}
    | FLEXIBLE d = VARIABLE {Flexible (Var d)}

%inline share_item:
    | FULLCOMPASS {Fullcompass}
    | CHIRALITY   {Chirality}
    | DIRECTION   {DirectionShare}
    | ONEAXEORIEN {OneAxisOrientation}

robogram:
    | expression * {$1}

expression:
    | e = constant {e}
    /*if e1 then e2 else e3*/
    | IF e1 = expression THEN e2 = expression  ELSE e3 = expression {Cond(e1,e2,e3)}  %prec AFFECT
    /*e1 binop e2 avec binop appartient a {+ ; - ; * ; / ; || ; &&; == ; != ; < ; > ;*/
    | e1 = expression op = binop e2 = expression {BinOp(e1,op,e2)}
    | NOT expression {Not $2}
    /*let name_variable = e in */
    | LET v = VARIABLE AFFECT e = expression IN e2=expression {Let(v,e,e2)} %prec AFFECT
    /*name_variable = e*/
    | v = VARIABLE; AFFECT e = expression {Affectation(v,e)}
    /*(a;...b;)*/
    | PARENOPEN s = set PARENCLOSE {Set s} 
    | MATCH PARENOPEN i= appfunc PARENCLOSE WITH p = patternmatch END {PatternMatching(i,p)}
    | MATCH  i= id  WITH p = patternmatch END {PatternMatching(i,p)}
    | f = deffun  {f}
    | f = appfunc {f} %prec VARIABLE  /*if conflict: if we can resolve it on the right we do it otherwise shift */
    | i = id {i} %prec VARIABLE 

constant :
    /*booleen*/
    | b = BOOLEAN {Bool b}
    /*entier*/
    | i = INT {Cst i}
    /*flottant*/
    | f = FLOAT {Cstf f}
    
deffun : 
    | FUN v = VARIABLE ARROW e = expression {Fun(v,e)} %prec AFFECT

appfunc :
    | i = id arg = argument { App(i, arg) } 
    | f = appfunc arg= argument {App(f,arg)} 
    
argument :  
    | constant  {$1}
    | i = id  {i} 
    | PARENOPEN deffun PARENCLOSE {$2}
    | PARENOPEN appfunc PARENCLOSE {$2}
    | PARENOPEN e1 = expression op = binop e2 = expression PARENCLOSE {BinOp(e1,op,e2)}
    | PARENOPEN NOT expression PARENCLOSE {Not $3}

id : 
    | VARIABLE {Var $1}
    | x = KEYWORD POINT e = VARIABLE {Point(x,e,$startpos.Lexing.pos_lnum)} 


set :
    | e = expression COMMA s = set {Some(s,e)} 
    | e = expression {Some(Vide,e)} 
    | e = KEYWORD COMMA s = set {Some(s,Var e)} 
    | e = KEYWORD {Some(Vide,Var e)} 
    | {Vide}

%inline binop :
    | PLUS {Plus}
    | MINUS {Minus}
    | MULT {Mult}
    | DIV {Div} 
    | SUP {Sup}
    | INF {Inf}
    | EQUAL {Equal}
    | DIFF {Diff}
    | AND {And}
    | OR {Or}


patternmatch : 
    | {[]}
    | PIPE p = pattern ARROW e = resultpattern  pm = patternmatch {(p,e)::pm} 
    
resultpattern :
    | expression {$1}

pattern :
    | v = VARIABLE {Var v}
    | v = VARIABLE COLON COLON e = pattern {Cons(Var v,e)}
    | PARENOPEN s = set PARENCLOSE COLON COLON e = pattern {Cons(Set s,e)}

measure:
    | el = edge_list {Measure(graph_from_edges (List.flatten el))}

edge : 
    | v1 = sommet ARROW v2 = sommet {[Nolab(v1,v2)]}
    | v = sommet ARROW BRACKETOPEN  sl = separated_list (SEMICOLON,sommet) BRACKETCLOSE {List.map (fun x -> Nolab(v,x))sl}
    
edge_list :
    | v = list(edge) { v }


sommet :
    | p = phase {p}

phase :
    | VARIABLE {Var $1}
    | KEYWORD {Var $1}
