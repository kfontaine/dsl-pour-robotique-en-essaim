type t =
  | Implicit (* implicit argument or anonymous pattern "_" *)
  | True
  | False
  | Cst of string
  | Int of int
  | Var of int * string (* DeBruijn Index + (mandatory) name *)
  | And of t*t
  | Or of t*t
  | InfixOp of t*string*t (* Generic operators *)
  | PrefOp of string*t (* Generic operators *)
  | Not of t
  | ExclMark of t
  | Imply of t*t
  | If of t * t * t
  | Forall of string * t
  | Exists of string * t
  | Let of string * t * t
  | Fun  of string option * t
  | App of t * t list
  | Infix of t * string * t
  | Match of t * (t * t) list
  | Record of t list
  | RecordDef of t list
  | Tuple of t list
  | Set of t list (*modif*)
  | Raw of string
[@@deriving show,eq,ord,yojson { strict = true }]

let priority f =
  match f with
  | Implicit -> 0
  | True -> 0
  | False -> 0
  | Cst _ -> 0
  | Int _ -> 0
  | InfixOp (_,_s,_) -> 110 (* TODO: have this configurable wrt s *)
  | PrefOp (_s,_) -> 200 
  | If (_,_,_) -> 100
  | Var (_,_) -> 0
  | Not (_) -> 105
  | ExclMark (_) -> 106
  | App (_,_) -> 107
  | And (_,_) -> 110
  | Or (_,_) -> 115
  | Imply (_,_) -> 120
  | Forall (_,_) -> 125
  | Exists (_,_) -> 125
  | Fun (_,_) -> 150
  | Match(_,_) -> 0
  | Record _ -> 0
  | RecordDef _ -> 0
  | Tuple(_) -> 0
  | Set(_) -> 0
  | Raw _ -> 0
  | Let (_,_,_) -> 0
  | Infix (_,_,_) -> 108 (* so that "a < 4 /\ A >= 9" works *)


(* returns true if f2 does not need parenthesis when directly under the head
   connector of f1 *)
let priority_cmp f1 f2 = priority f1 >= priority f2
let priority_cmpstrict f1 f2 = priority f1 > priority f2

let pr_str_opt fmt sopt =
  match sopt with
  | Some s -> Format.fprintf fmt "%s" s
  | None -> Format.fprintf fmt "_"

let rec pretty fmt form =
  let p fmt f = Format.fprintf fmt "(%a)" pretty f in
  let fp fmt f = Format.fprintf fmt "%a" pretty f in
  match form with
  | Implicit -> Format.fprintf fmt "_"
  | True -> Format.fprintf fmt "True"
  | False -> Format.fprintf fmt "False"
  | Cst s ->  Format.fprintf fmt "%s" s
  | Int n ->  Format.fprintf fmt "%d" n
  | Var (_,s) -> Format.fprintf fmt "%s" s (* TODO: rename hidden variables *)
  | And (f1,f2) ->
     let parf1 = if priority_cmp form f1 then fp else p in
     let parf2 = if priority_cmp form f2 then fp else p in
     Format.fprintf fmt "@[<hov 2>%a@ /\\@ @[<hov 2>%a@]@]" parf1 f1 parf2 f2
  | InfixOp (f1,op,f2) ->
     let parf1 = if priority_cmp form f1 then fp else p in
     let parf2 = if priority_cmp form f2 then fp else p in
     Format.fprintf fmt "@[<hov 2>%a@ %s@ @[<hov 2>%a@]@]" parf1 f1 op parf2 f2
  | PrefOp (op,f1) ->
     let parf1 = if priority_cmp form f1 then fp else p in
     Format.fprintf fmt "@[<hov 2>%s@ @[<hov 2>%a@]@]" op parf1 f1
  | Or (f1,f2) ->
     let parf1 = if priority_cmp form f1 then fp else p in
     let parf2 = if priority_cmp form f2 then fp else p in
     Format.fprintf fmt "@[<hov 2>%a@ \\/@ @[<hov 2>%a@]@]" parf1 f1 parf2 f2
  | If (f1,f2,f3) ->
     let parf1 = if priority_cmp form f1 then fp else p in
     let parf2 = if priority_cmp form f2 then fp else p in
     let parf3 = if priority_cmp form f2 then fp else p in
     Format.fprintf fmt "@[<hov 2>if %a@ then %a@ else %a@]" parf1 f1 parf2 f2 parf3 f3
  | Infix ((App(Cst "Notation",_))as f1,":=",f2) ->
     (* Special treatment for notations: we need a parenthesis on the right. *)
     let parf1 = if priority_cmp form f1 then fp else p in
     let parf2 =  p in
     Format.fprintf fmt "@[<hov 2>%a@ %s@ @[<hov 2>%a@]@]" parf1 f1 ":=" parf2 f2

  | Infix (f1,op,f2) ->
     let parf1 = if priority_cmp form f1 then fp else p in
     let parf2 = if priority_cmp form f2 then fp else p in
     Format.fprintf fmt "@[<hov 2>%a@ %s@ @[<hov 2>%a@]@]" parf1 f1 op parf2 f2
  | Not (f1) ->
     let parf1 = if priority_cmp form f1 then fp else p in
     Format.fprintf fmt "@[<hov 2>~ %a@]" parf1 f1
  | ExclMark (f1) ->
     let parf1 = if priority_cmp form f1 then fp else p in
     Format.fprintf fmt "@[<hov 2>!%a@]" parf1 f1
  | Imply (f1,f2) ->
     let parf1 = if priority_cmpstrict form f1 then fp else p in
     let parf2 = if priority_cmp form f2 then fp else p in
     Format.fprintf fmt "@[<hov 2>%a@ ->@ @[<hov 2>%a@]@]" parf1 f1 parf2 f2
  | Raw (s) -> Format.fprintf fmt "%s" s
  | Forall (s,f2) ->
     let parf2 = if priority_cmp form f2 then fp else p in     
     Format.fprintf fmt "@[<hov 2>forall %s,@ %a@]" s parf2 f2
  | Exists (s,f2) ->
     let parf2 = if priority_cmp form f2 then fp else p in     
     Format.fprintf fmt "@[<hov 2>exists %s,@ %a@]" s parf2 f2
  | Fun (s,f2) ->
     let parf2 = if priority_cmp form f2 then fp else p in     
     Format.fprintf fmt "@[<hov 2>fun %a =>@ %a@]"
       pr_str_opt s parf2 f2
  | App (f1,lf) ->
     let parf1 = if priority_cmp form f1 then fp else p in
     Format.fprintf fmt "@[%a@ %a@]" parf1 f1 (pretty_list form) lf
  | Record (lst) -> Format.fprintf fmt "@[<v 2>{| %a |}@]" (Pp.print_list Pp.semi fp) lst
  | RecordDef (lst) -> Format.fprintf fmt "@[<v 2>{ %a }@]" (Pp.print_list Pp.semi fp) lst
  | Tuple (lst) -> Format.fprintf fmt "@[{ %a }@]" (Pp.print_list Pp.comma fp) lst
  | Set (lst) -> Format.fprintf fmt "@[( %a )@]" (Pp.print_list Pp.comma fp) lst
  | Let(id, f1, f2) -> Format.fprintf fmt "@[<v>let %s := %a@ in @[<v>%a@]@]"
                       id 
                       fp f1 
                       fp f2
  | Match(f,lcase) -> Format.fprintf fmt "@[<v>match %a with@,@[<v>%a@]@,end@]"
                        fp f
                        (Pp.print_list Pp.brk pretty_case) lcase
and pretty_list head fmt lf =
  let p fmt f = Format.fprintf fmt "(%a)" pretty f in
  let fp fmt f = Format.fprintf fmt "%a" pretty f in
  match lf with
  | [] -> Format.fprintf fmt ""
  | e::[] -> (* Avoid the trailing space *)
     let pare = if priority_cmpstrict head e then fp else p in     
     Format.fprintf fmt "%a" pare e
  | e::lf' ->
     let pare = if priority_cmpstrict head e then fp else p in     
     Format.fprintf fmt "%a@ %a" pare e (pretty_list head) lf'
and pretty_case fmt (f1,f2) =
  let fp fmt f = Format.fprintf fmt "%a" pretty f in
  Format.fprintf fmt "@[<hov 2>| %a@ =>@ %a@]" fp f1 fp f2

let to_string f: string =
  Format.fprintf Format.str_formatter "%a" pretty f
  ;Format.flush_str_formatter()
